
plenaries = """

\\section*{Plenary Lectures}


\\begin{tabular}{|p{117mm}|}\\hline
""" + 'Magnetron sputtering of nanomaterials for various applications'.upper() + """\\newline
\\textbf{Vadym Prysiazhnyi}, Dr. Sc., Institute of Physics and Biophysics, Faculty of Science, University of South Bohemia
\\\\\\hline
""" + 'Numerical modeling of electromagnetic scattering by discrete random media'.upper() + """\\newline
\\textbf{Janna Dlugach}, Dr. Sc. Senior scientist in Main Astronomical Observatory of NANU
\\\\\\hline
""" + 'Куда стремятся сетевые технологии, и роль Cisco в этих процессах'.upper() + """\\newline
\\textbf{Юрий Довгань}, Cisco Systems, Inc.
\\\\\\hline
""" + 'xxx'.upper() + """\\newline
\\textbf{Xxx}, Xxx
\\\\\\hline
\\end{tabular}

\\section*{Career Development Workshop in IT Industry and Science}


\\begin{tabular}{|p{117mm}|}\\hline
""" + 'Magnetron sputtering of nanomaterials for various applications'.upper() + """\\newline
\\textbf{Vadym Prysiazhnyi}, Dr. Sc., Institute of Physics and Biophysics, Faculty of Science, University of South Bohemia
\\\\\\hline
""" + 'Numerical modeling of electromagnetic scattering by discrete random media'.upper() + """\\newline
\\textbf{Janna Dlugach}, Dr. Sc. Senior scientist in Main Astronomical Observatory of NANU
\\\\\\hline
\\end{tabular}





"""

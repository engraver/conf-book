from abc import ABC, abstractmethod

class Engine(ABC):
	
	def __init__(self, fid, tracks, abstracts, authors):
		self.fid = fid
		self.tracks = tracks
		self.abstracts = abstracts
		self.authors = authors

	@classmethod
	@abstractmethod
	def buildHead(self):
		pass
	
	@classmethod
	@abstractmethod
	def buildBody(self):
		pass
		
	@classmethod
	@abstractmethod
	def buildTail(self):
		pass
	
	def securestr(self, s):
		return s.replace('_', '\\_')

class Track:
	
	def __init__(self, pid, title, code):
		self.pid   = pid
		self.title = title
		self.code  = code
	
	def __str__(self):
		return self.title + ' (' + self.code + ')'
	 
	def __repr__(self):
		return 'Track({title})'.format(title=self.title)
	
	def toLaTeX(self):
		return self.title

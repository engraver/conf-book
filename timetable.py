
timetable = """
\\begin{adjustbox}{angle=90}
\\centering
\\begin{tabular}{cllcp{80mm}}\\hline
\\textbf{Date} & \\textbf{Time} & \\textbf{Place} & \\multicolumn{2}{c}{\\textbf{Activity}} \\\\\\hline
Tue 22/05 & 9:00 -- 14:00 & Hall & \\multicolumn{2}{l}{Arrival/Registration} \\\\\\hline

\\parbox[t]{2mm}{\\multirow{6}{*}{\\rotatebox[origin=c]{90}{Wed 23/05}}} &  9:00--12:10 & Hall & \\multicolumn{2}{l}{Registration} \\\\\\cline{2-5}
                           & 12:30--14:00 & Plen.\\,Hall\\,\\#42 & \\multicolumn{2}{l}{\\textbf{Plenary Lectures}} \\\\\\cline{2-5}
                           & 14:00--14:30 & Hall & \\multicolumn{2}{l}{Coffee-break/Lunch} \\\\\\cline{2-5}
                           & \\multirow{3}{*}{14:30--17:00} & Room\\,\\#XX & MS  &  Magnetism and Superconductivity \\\\\\cline{3-5}
                           &                                & Room\\,\\#XX & LPO &  Laser Physics and Optoelectronics \\\\\\cline{3-5}
                           &                                & Room\\,\\#XX & MP  & Medical Physics \\\\\\hline\\hline
\\parbox[t]{2mm}{\\multirow{8}{*}{\\rotatebox[origin=c]{90}{Thu 24/05}}} &  9:00--10:30 & Hall & \\multicolumn{2}{l}{Registration} \\\\\\cline{2-5}
                           & 10:30--12:20 & Plen.\\,Hall\\,\\#XX & \\multicolumn{2}{l}{\\textbf{Career Development Workshop in IT Industry and Science}} \\\\\\cline{2-5}
                           & 12:20--12:50 & Hall & \\multicolumn{2}{l}{Coffee-break/Lunch} \\\\\\cline{2-5}
                           & 12:50--14:00 & Hall & \\multicolumn{2}{l}{\\textbf{Posters/Coffee}} \\\\\\cline{2-5}
                           & \\multirow{2}{*}{14:00--16:30} & ICC\\,Hall   & CE  & Computer Engineering \\\\\\cline{3-5}
                           &                                & Room\\,\\#XX & REC & Radio Engineering and Communication \\\\\\cline{3-5}
                           & 16:30--19:00 & & \\multicolumn{2}{l}{Social Program} \\\\\\hline\\hline
\\parbox[t]{2mm}{\\multirow{12}{*}{\\rotatebox[origin=c]{90}{Fri 25/05}}} &  9:00--10:30 & Hall & \\multicolumn{2}{l}{Registration} \\\\\\cline{2-5}
                           & \\multirow{2}{*}{10:30--12:30} & Room\\,\\#XX & PP & Plasma Physics \\\\\\cline{4-5}
%                          &                                & Room\\,\\#XX & SP & Surface Physics and Nanoelectronics \\\\\\cline{2-5}
                           &                                & Room\\,\\#XX & PS & Physics of Semiconductors and Dielectrics, Semiconductor's Devices \\\\\\cline{3-5}
                           & 12:30--13:00 & Hall & \\multicolumn{2}{l}{Coffee-break/Lunch} \\\\\\cline{2-5}
                           & \\multirow{3}{*}{13:00--15:00} & Room\\,\\#XX & PP & Plasma Physics (PP) \\\\\\cline{4-5}
                           &                                & Room\\,\\#XX & PS & Physics of Semiconductors and Dielectrics, Semiconductor's Devices \\\\\\cline{3-5}
                           & 15:00--15:30 & Plen.\\,Hall\\,\\#42 & \\multicolumn{2}{l}{\\textbf{Closing Ceremony}} \\\\\\cline{2-5}
                           & 15:30--18:00 & & \\multicolumn{2}{l}{\\textbf{Closing Party}} \\\\\\hline
Sat 26/05 &  &  & \\multicolumn{2}{l}{Departure} \\\\\\hline
\\end{tabular}
\\end{adjustbox}
"""

from engine import Engine

import timetable
import plenaries
import confdata

class LaE1(Engine):
	
	date = confdata.date
	title = confdata.title
	email = confdata.email
	url = confdata.url
	
	def __init__(self, fid, tracks, abstracts, authors):
		super(LaE1, self).__init__(fid, tracks, abstracts, authors)

	def buildHead(self):
		self.fid.write(("""\\documentclass[a5paper, twoside, 10pt]{article}
\\usepackage[T1, T2A]{fontenc}
\\usepackage[utf8x]{inputenc}
\\usepackage[ukrainian,russian,english]{babel}
\\usepackage[top=2cm, bottom=1cm, left=1cm, right=1.5cm]{geometry}
%\\pdfpageheight=210mm
%\\pdfpagewidth=148mm

\\usepackage{longtable,multirow}
\\usepackage{booktabs}
\\usepackage{rotating,adjustbox}

\\usepackage{fancyhdr}
\\pagestyle{fancy}
\\fancyhf{} % sets both header and footer to nothing
\\renewcommand{\\headrulewidth}{0pt}
\\fancyhead{}
\\fancyhead[LE,RO]{\\thepage}
\\renewcommand{\\sectionmark}[1]{\\markright{#1}}
%\\fancyhead[LE]{\\thepage | \\rightmark}
%\\fancyhead[RO]{\\rightmark | \\thepage}
\\fancyhead[C]{\\rightmark}
\\fancyfoot{}

\\fancypagestyle{emptycolon}%
{%
\\fancyhead{}
\\fancyfoot{}
}


\\thispagestyle{emptycolon}
\\usepackage[pages=some]{background}
\\backgroundsetup{%
 scale=1
,angle=0
,opacity=1
,contents={%
 \\includegraphics[width=\\paperwidth,height=\\paperheight]{titlePage}
 }
}


\\usepackage{pdfpages}
\\usepackage[normalem]{ulem}
\\renewcommand{\\ULthickness}{0.4pt}
\\usepackage{multicol}
\\usepackage{enumitem}

\\newcounter{oralsCounter}
\\newcounter{postersCounter}

\\begin{document}
\\thispagestyle{emptycolon}
\\BgThispage
\\newpage\\null\\newpage
\\clearpage\\thispagestyle{emptycolon}\\setcounter{page}{1}
%\\clearpage

\\begin{center}
\\begin{tabular}{|p{17mm}|p{90mm}|}\\hline
\\textbf{Date}      & """ + self.date + """ \\\\\\hline
\\textbf{Organizer} & Taras Shevchenko National University of Kyiv\\newline
                     Faculty of Radio Physics, Electronics and Computer Systems \\\\\\hline
\\textbf{Venue}     & Acad. Glushkov ave., 4g, Kyiv, Ukraine \\\\\\hline
\\end{tabular}
\\end{center}

\\section*{Conference Tracks}

\\stepcounter{oralsCounter}
\\stepcounter{postersCounter}

\\begin{enumerate}
""").encode('UTF-8'))
		for t in self.tracks:
			s = '\\item ' + str(t) + '\n'
			self.fid.write(s.encode('UTF-8'))
		self.fid.write(("""
\\end{enumerate}

\\section*{Contacts}
""" + self.title + """\\newline
Faculty of Radio Physics, Electronics and Computer Systems\\newline
of Taras Shevchenko National University of Kyiv\\newline
64/13, Volodymyrska Str., Kyiv, Ukraine, 01601\\newline
e-mail: """ + self.email + """\\newline
""" + self.url + """

%\\includepdf[pages={1}]{poster.pdf}
\\newpage
\\section*{Location}
\\begin{center}
\\includegraphics[height=110mm,angle=90]{map}\\newline
\\end{center}

%\\newpage

""" + timetable.timetable + """

\\newpage

""" + plenaries.plenaries + """
""").encode('UTF-8'));
		
	def buildTail(self):
		self.fid.write("""

\\newpage\\thispagestyle{emptycolon}
\\section*{Sponsors}
\\begin{center}
\\includegraphics[width=130mm]{sponsors/lunua}\\\\%[10mm]
\\includegraphics[width=50mm]{sponsors/spu}
\\includegraphics[width=50mm]{sponsors/uft}\\\\[10mm]
\\includegraphics[width=40mm]{sponsors/rada}
\\end{center}

\\newpage\\thispagestyle{emptycolon}
\\section*{Notes}
	
\\end{document}
""".encode('UTF-8'));

	def _abstract2LaTeX(self, a):
		secstr = super(LaE1, self).securestr
		s = secstr(a.title) + '\\newline\n\t\t'
		affil = a.authors[0][0].affiliation
		for a in a.authors:
			auth = a[0]
			try:
				affPrinted = False
				if auth.affiliation != affil:
					s += ' \\textit{{{}}}'.format(affil)
					affil = auth.affiliation
					affPrinted = True
				if affPrinted:
					s += ', '
				if auth.speaker:
					s += ' \\underline{{\\textbf{{{first}.~{last}}}}}'.format(first=auth.first[0], last=auth.last)
				else:
					s += ' \\textbf{{{first}.~{last}}}'.format(first=auth.first[0], last=auth.last)
				if len(auth.email) > 0:
					email = auth.email
					s += ' (' + secstr('\-'.join(email[i:i+2] for i in range(0, len(email), 2))) + ')'
			except:
				print('Problem with ', auth)
			s += ', '
		s += ' \\textit{{{}}}'.format(affil)
		return s

	def _contribution(self, abst, item):
		if len(abst) > 0:
			self.fid.write('\\begin{itemize}\n'.encode('UTF-8'))
			for a in abst:
				self.fid.write(('\t' + item + ' ' + self._abstract2LaTeX(a) + ' \n').encode('UTF-8'))
			self.fid.write('\\end{itemize}\n\n'.encode('UTF-8'))
		return len(abst)

	def contributions(self):
		for t in self.tracks:
			self.fid.write(('\\section{' + str(t) + '}\n\n').encode('UTF-8'))
			
			self.fid.write(('\\textbf{Chairman:} ???\n\n').encode('UTF-8'))
			
			abst = [elem for elem in self.abstracts if elem.track == t and elem.contribType.lower() == 'oral']
			cnt = self._contribution(abst, '\item[O\\arabic{oralsCounter}]\\stepcounter{oralsCounter}')
			
			abst = [elem for elem in self.abstracts if elem.track == t and elem.contribType.lower() == 'poster']
			if len(abst) > 0 and cnt > 0:
				self.fid.write('\\sout{\\hfill}\n\n'.encode('UTF-8'))
			cnt = self._contribution(abst, '\item[P\\arabic{postersCounter}]\\stepcounter{postersCounter}')
	
	def authorsList(self):
		self.fid.write('\n\\section*{Authors}\n\n'.encode('UTF-8'))
		self.fid.write('\\setlist[enumerate]{itemsep=-1mm}\n\\begin{multicols}{3}\n\\noindent '.encode('UTF-8'))
		auths = sorted([elem[1] for elem in self.authors.items()], key=lambda x: x.last)
		for a in auths:
			self.fid.write('{l},~{f}.\\newline\n'.format(f=a.first[0], l=a.last).encode('UTF-8'))			
		
		self.fid.write('\\end{multicols}\n'.encode('UTF-8'))
		
	def institutionsList(self):
		self.fid.write('\n\\section*{Institutions}\n\n'.encode('UTF-8'))
		self.fid.write('\\begin{enumerate}\n '.encode('UTF-8'))
		institutions = sorted(list(set([elem[1].affiliation for elem in self.authors.items()])))
		for af in institutions:
			self.fid.write('\t\\item {aff}\n'.format(aff=af).encode('UTF-8'))
		
		self.fid.write('\\end{enumerate}'.encode('UTF-8'))
	
	def buildBody(self):
		self.contributions()
		self.authorsList()
		self.institutionsList()

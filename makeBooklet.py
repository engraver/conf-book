#!/usr/bin/python3
# -*- coding: utf-8 -*-
from sys import exit
from lae1 import LaE1
from conftracks import *
from booklet import Booklet

def mymain():
	print('Start work...')
	booklet = Booklet('abstracts.json', 'booklet-test.tex', tracks, LaE1)
	print('Loading data...')
	if booklet.load():
		print('Building booklet...')
		booklet.build()
		print('Printing emails of participants to \'emails.txt\'...')
		with open('emails.txt', 'w') as f:
			f.write('\n'.join(booklet.getMails()))
		print('Finish! Start pdflatex now.')

if __name__ == "__main__":
	mymain();

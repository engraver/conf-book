# Overview
This set of Python scripts generates a conference booklet from a JSON dump of [Indico](https://indico.cern.ch/) database as a LaTeX file. 
Scripts are tested with Python 3.5 and Indico of 2.1 version (should also work with 1.9.11.dev15).

[Indico](https://indico.cern.ch/) is a powerful tool for events management and we use it for [conferences](https://indico.knu.ua/category/1/) hold by Faculty of Radio Physics of Taras Shevchenko National University of Kyiv.
Management area of the event contains chapter 'Call for Abstracts' with 'Manage' button. The last one provides the list of all submissions for the given event. Export to JSON is available if at least a few submissions are selected.

Event contains different tracks. They should be described in `conftracks.py` as a list of `Track` objecs (see `track.py` module).

General booklet structure is described in child classes of `Engine` events. Here `LaE1` is provided (see `lae1.py`). 
The most frequently changed info is moved to `confdata.py` (dates, event caption, e-mail of ogranizing committee, address).
Plenary lecturers are described in `plenaries.py`.
Timetable is described in `timetable.py`.
All these files contain a Python string with LaTeX content in arbitrary form. They are substituted into the given positions of the further booklet in `LaE1` engine.

Example how to generate a booklet is shown in `makeBooklet.py` using `abstracts.json` database for producing `booklet-test.tex`. 
Of course, final processing of booklet as further publication should be done in TeX (e.g. word breaks, starting sections from new pages). 
Note, that e-mails break in line endings each second symbol automatically in `LaE1` engine.
Thus we recommend to keep file name `booklet-test.tex` and copy it to e.g. `booklet-final.tex` to finish the work.

# ToDo

1. Print stats like total number of oral and poster contributions
1. Use IDs from Indico to get more close relation with online information.
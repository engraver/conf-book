import json as js
from pprint import pprint
from abstract import Abstract
from track import Track
from person import Person
from sys import stderr
from io import StringIO

########################################################################


class InvalidJSONElement(Exception):
	def __init__(self, elem, msg = ''):
		self.json = elem
		self.msg  = msg
	
	def __str__(self):
		s = StringIO()
		pprint(self.json, s)
		return 'Description: {msg}\n\nElement:\n{elem}'.format(msg=self.msg, elem=s.getvalue())
	
	def __repr__(self):
		return self.__str__()

########################################################################


class Booklet:
	
	def __init__(self, jname, bname, tracks, engineClass):
		"""Initializes booklet generator stuff. 
		
		Args:
		    jname (str): filename of JSON with database (including extension)
		    bname (str): filename of further booklet (including .tex extension)
		    tracks (list): list of Track objects
		    engineClass (class): reference to Engine generating booklet
		"""
		self.jname = jname
		self.bname = bname
		self.tracks = tracks
		self.data = []
		self.author = {}
		self.abstract = []
		self.engineClass = engineClass
		
	def load(self, onlyJudged=True):
		"""Loads JSON database from jname file.
		
		Can raise exceptions: NotImplementedError, InvalidJSONElement
		
		Args:
		    onlyJudged (bool): load only judged submissions, True by default
		
		Returns:
		    bool: True in success
		"""
		with open(self.jname) as data_file:
			dataAll = js.load(data_file)
		if onlyJudged:
			self.data = [elem for elem in dataAll['abstracts'] if elem['state'] == 'accepted']
		else:
			raise NotImplementedError()
		
		try:
			self._construct()
		except InvalidJSONElement as e:
			print(e)
			return False
		return True
	
	def _getTrack(self, code):
		"""Returns track object by code or None if not found.
		
		Args:
		    code (str): track code
		    
		Returns:
		    Track: track found
		"""
		for t in self.tracks:
			if t.code == code:
				return t
		return None
	
	def _construct(self):
		"""Fills class fields for authors and abstracts.
		
		Raises InvalidJSONElement.
		"""
		for elem in self.data:			
			if not 'title' in elem.keys():
				raise InvalidJSONElement(elem, "Absent 'title' key!")
			if not 'content' in elem.keys():
				raise InvalidJSONElement(elem, "Absent 'content' key!")
			if not 'accepted_track' in elem.keys():
				raise InvalidJSONElement(elem, "Absent 'accepted_track' key!")
			if not 'code' in elem['accepted_track'].keys():
				raise InvalidJSONElement(elem, "Absent 'accepted_track' 'code' key!")
			if not 'accepted_contrib_type' in elem.keys():
				raise InvalidJSONElement(elem, "Absent 'accepted_contrib_type' key!")
			if not 'name' in elem['accepted_contrib_type'].keys():
				raise InvalidJSONElement(elem, "Absent 'accepted_contrib_type' 'name' key!")
				
			title = elem['title']
			content = elem['content']
			track = self._getTrack(elem['accepted_track']['code'])
			contrib = elem['accepted_contrib_type']['name']
			
			abst = Abstract(title
						 , content
						 , track
						 , contrib
						   )
			
			if not 'persons' in elem.keys():
				raise InvalidJSONElement(elem, "Absent 'persons' key!")
			
			for p in elem['persons']:
				if not 'person_id' in p.keys():
					raise InvalidJSONElement(elem, "Absent 'person_id' key!")
				if not 'title' in p.keys():
					raise InvalidJSONElement(elem, "Absent 'title' key!")
				if not 'first_name' in p.keys():
					raise InvalidJSONElement(elem, "Absent 'first_name' key!")
				if not 'last_name' in p.keys():
					raise InvalidJSONElement(elem, "Absent 'last_name' key!")
				if not 'email' in p.keys():
					raise InvalidJSONElement(elem, "Absent 'email' key!")
				if not 'affiliation' in p.keys():
					raise InvalidJSONElement(elem, "Absent 'affiliation' key!")
				if not 'address' in p.keys():
					raise InvalidJSONElement(elem, "Absent 'address' key!")
				if 'is_speaker' in p.keys():
					if p['is_speaker']:
						speaker = True
					else:
						speaker = False
				else:
					speaker = False
					
				pid = int(p['person_id'])
				if not pid in self.author.keys():
					pers = Person(pid
									, p['title']
									, p['first_name']
									, p['last_name']
									, p['email']
									, p['affiliation']
									, p['address']
									, speaker
									  )
					self.author[pid] = pers
				abst.addAuthor(self.author[pid])
				
			self.abstract.append(abst)
		self.checkAuthors()
	
	def checkAuthors(self):
		"""Checks validity of author's data in the class `author` field.
		
		Result is printed to stderr. The following is controlled:
		* No first name given
		* No last name given
		* No affiliation given
		* Persons with the same last name and the same first letters of 
		  first names are reported as possible duplicates.
		"""
		values = list(self.author.values())
		for p in values:
			if len(p.first) == 0:
				print('No first name in ' + str(p), file=stderr)
			if len(p.last) == 0:
				print('No last name in ' + str(p), file=stderr)
			if len(p.affiliation) == 0:
				print('No affiliation name in ' + str(p), file=stderr)
		authorNames = [p.last + p.first[0] for p in self.author.values()]
		forFinding  = authorNames[:]
		foundPossibleDuplicates = []
		for a in authorNames:
			indices = [i for i,a1 in enumerate(forFinding) if a1 == a]
			if len(indices) > 1:
				print('The following authors are possibly the same persons:', file=stderr)
				for i in indices:
					print('\t{}'.format(values[i]))
					forFinding[i] = '______'
	
	def build(self):
		"""Builds booklet in tex format.
		"""
		self.fid = open(self.bname, 'wb')
		self.engine = self.engineClass(self.fid, self.tracks, self.abstract, self.author)
		self.engine.buildHead()
		self.engine.buildBody()
		self.engine.buildTail()
		self.fid.close()
	
	def getMails(self):
		"""Returns list of mails of all registered participants
		"""
		mails = set()
		for a in self.author.values():
			mails.add(a.email)
		return list(mails)

class Person:
	
	def __init__(self, pid, title, first, last, email, affiliation, address, speaker: False):
		self.pid   = pid
		if len(title) > 0:
			title = title.strip()
			self.title = title[0].upper() + title[1:].lower()
		else:
			self.title = "Dr."
		if len(first) > 0:
			first = first.strip()
			self.first = first[0].upper() + first[1:]
		else:
			raise ValueError('Empty first name for person with id ' + str(pid))
		last = last.strip()
		self.last  = last[0].upper() + last[1:].lower()
		self.email = email.strip().lower()
		self.affiliation = affiliation.strip()
		self.address = address.strip()
		self.speaker = speaker
		
	def __eq__(self, other):
		if not other is Person:
			return False
		
		return (self.pid == other.pid) and (self.first == other.first) and (self.last == other.last) and (self.email == other.email)
	
	def toLaTeX(self):
		if self.speaker:
			return '\\underline{\\textbf{{title}.~{F}.\\,{last}} (email)}'.format(title=self.title, F=self.first[0], last=self.last, email=self.email)
		else:
			return '\\textbf{{title}.~{F}.\\,{last} (email)}'.format(title=self.title, F=self.first[0], last=self.last, email=self.email)
	
	def __repr__(self):
		s = 'Author({}: {} {} {}'.format(self.pid, self.title, self.first, self.last)
		if len(self.email) > 0:
			s += ' ({})'.format(self.email)
		s += ', {}'.format(self.affiliation)
		if self.speaker:
			s += ', Speaker'
		else:
			s += ', - '
		s += ')'
		return s

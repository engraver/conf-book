from track import Track

tracks = sorted(
         [Track(10
              , "Laser Physics and Optoelectronics"
              , "LPO"
              )
        , Track(11
              , "Magnetism and Superconductivity"
              , "MS"
              )
        , Track(12
              , "Surface Physics and Nanoelectronics"
              , "SP"
              )
        , Track(13
              , "Physics of Semiconductors and Dielectrics, Semiconductor's Devices"
              , "PS"
              )
        , Track(14
              , "Medical Physics"
              , "MP"
              )
        , Track(15
              , "Plasma Physics"
              , "PP"
              )
        , Track(16
              , "Computer Engineering"
              , "CE"
              )
        , Track(17
              , "Radio Engineering and Communications"
              , "REC"
              )
         ]
        , key = lambda x: x.pid
        )

class Abstract:
	
	def __init__(self, title, content, track, contribType):
		self.title = title.upper().strip()
		if self.title[-1] == '.':
			self.title = self.title[:-1]
		self.content = content
		self.track = track
		self.authors = []
		self.contribType = contribType
	
	def addAuthor(self, author, speaker=False):
		self.authors.append([author, speaker])
		#self.authors = sorted(self.authors, key = lambda x: x[1], reverse=True)
	
	def getSpeaker(self):
		for a,speaker in self.authors:
			if speaker:
				return a
	
	def __getitem__(self, c):
		if c == 'speaker':
			return self.getSpeaker()
	
	def __repr__(self):
		return 'Abstract({title}, {contribType})'.format(title = self.title, contribType = self.contribType)
		
